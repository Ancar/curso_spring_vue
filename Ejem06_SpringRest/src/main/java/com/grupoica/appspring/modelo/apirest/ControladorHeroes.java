package com.grupoica.appspring.modelo.apirest;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.grupoica.appspring.modelo.daos.HeroesDAO;
import com.grupoica.appspring.modelo.entidades.Heroe;

@RestController
@RequestMapping("/api/heroes")
@CrossOrigin
public class ControladorHeroes {
	
	@Autowired
	HeroesDAO heroesDAO;
	
	@RequestMapping(value="/heroe", method=RequestMethod.GET)
	public Heroe leerHeroe() {
		Heroe heroe = new Heroe();
		heroe.setNombre("Viuda Negra");
		return heroe;
	}
	@PostMapping
	public Heroe guardarHeroe(@RequestBody Heroe nuevoHeroe) {
		System.out.println(">>>>> Heroe recibido" + nuevoHeroe.getNombre());
		return heroesDAO.save(nuevoHeroe);
	}
	@PutMapping
	public Heroe actualizarHeroe(@RequestBody Heroe nuevoHeroe) {
		if(nuevoHeroe.getId() > 0) {
			System.out.println(">>>>> Heroe recibido" + nuevoHeroe.getNombre());
			return heroesDAO.save(nuevoHeroe);
		}else {
			System.out.println(">>>>> Heroe sin ID" + nuevoHeroe.getNombre());
			return null;
		}
		
	}
	@DeleteMapping
	public void borrarHeroe(@RequestBody Heroe nuevoHeroe) {
		if(nuevoHeroe.getId() > 0) {
			System.out.println(">>>>> Heroe recibido" + nuevoHeroe.getNombre());
			heroesDAO.delete(nuevoHeroe);
		}else {
			System.out.println(">>>>> Heroe sin ID" + nuevoHeroe.getNombre());
			
		}
		
	}
	@RequestMapping(value="{id}", method=RequestMethod.DELETE)
	public void borrarHeroe(@PathVariable int id) {
		if(id > 0) {
			System.out.println(">>>>> Heroe recibido"+id);
			heroesDAO.deleteById(id);
		}else {
			System.out.println(">>>>> Heroe sin ID" +id);
			
		}
		
	}
	@RequestMapping(value="{id}", method=RequestMethod.GET)
	public Heroe getUno(@PathVariable int id) {
		if(id > 0) {
			System.out.println(">>>>> Heroe recibido"+id);
			return heroesDAO.findById(id).get();
		}else {
			System.out.println(">>>>> Heroe sin ID" +id);
			return null;
			
		}
		
	}
	@RequestMapping(method=RequestMethod.GET)
	public List<Heroe> getAll() {
		
		return heroesDAO.findAll();
	}

}
