package com.grupoica.appspring.modelo;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.context.SpringBootTest;

import com.grupoica.appspring.modelo.daos.HeroesDAO;
import com.grupoica.appspring.modelo.entidades.Heroe;

//@SpringBootTest

@DataJpaTest
class ModeloApplicationTests {
	
	@Autowired
	HeroesDAO heroesDAO;
	
	@Test
	void contextLoads() {
		Heroe ironMan = new Heroe();
		ironMan.setNombre("Ironman");
		Heroe spiderMan = new Heroe();
		spiderMan.setNombre("Spiderman");
		Heroe superMan = new Heroe();
		superMan.setNombre("Superman");
		heroesDAO.save(ironMan);
		heroesDAO.save(spiderMan);
		heroesDAO.save(superMan);
		System.out.println(heroesDAO.findAll().get(0).getNombre());
		
		assertThat(heroesDAO.findAll().get(0)).isEqualTo("Spiderman");
		
	}

}
