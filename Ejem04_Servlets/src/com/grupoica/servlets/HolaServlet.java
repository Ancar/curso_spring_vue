package com.grupoica.servlets;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class HolaServlet
 */
@WebServlet("/hola")
public class HolaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public HolaServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String strNombre = request.getParameter("nombre");
		String html = "<html><head><title>Formulario de envio</title></head>"
				+ "<body>";
		
		if(strNombre == null || "".equals(strNombre)) {
			html +="<h2>Pon el nombre</h2>";
		}else {
			html += "<form action='./hola.do' method='post'>" + 
					"Veces: <input name = 'veces' type = 'number'/>	"
					+ "<input type='submit' value='POST'/>" + 
					"</form>";
		}
		html+="</body></html>";
		response.getWriter().append(html);
	//	response.getWriter().append(html).append(request.getContextPath());
	}
	
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		try(PrintWriter escritor = response.getWriter()){
			String cuerpo = request.getParameter("veces");
			escritor.println("<html><head><title>Formulario de envio</title></head><body>");
			if(cuerpo == null || "".equals(cuerpo)) {
				System.out.println("El usuario no ha puesto datos");
				escritor.println("<h2>Falta n� de veces</h2>");
			}else {
				System.out.println("Usuario ha puesto " + cuerpo + " veces");
				int v = Integer.parseInt(cuerpo);
				escritor.println("<ul>");
				for(int i = 0; i<v; i++) {
					escritor.println("<li>"+i+"</li>");
				}
				escritor.println("</ul>");
			}
			escritor.println("</body></html>");
		}
		
	}

}
