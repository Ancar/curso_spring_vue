Vue.component("app-saludo", {
    "template": `<h2>Hola soy un componente</h2>`
});
Vue.component("app-despedida", {
    "template": `<h3>Me despido de ti</h3>`
})
new Vue({
    "el": "#app-section",
    template: `<div>
                    <h2>Hola ICA </h2>
                    <app-saludo></app-saludo>
                </div>`
});
new Vue({
    el: "#app-section-2"
})

new Vue({
    el: "#app-section-3",
    template: `<div>
                    <h2>Soy un componente chulo</h2>
                    <app-despedida></app-despedida>
                </div>`
})