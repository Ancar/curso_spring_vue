const PI = 3.1416;

let unaVar = 20;
let unTexto = "Que pasa!";
document.write(`<br> Texto en varias lineas
y ademas podemos mostrar <br> variables asi: ${unaVar}, ${unTexto}`);

//FUNCIONES LAMBDA O TAMBIEN LLAMADAS FUNCIONES ANONIMAS O EN JS FUNCIONES FLECHAS
//SIN PARENTESIS SOLO SI ES UN UNICO PARAMETRO EL QUE PASAMOS, EL RESTO
//SIEMPRE CON PARENTESIS
var suma = (x, y) => x + y;

document.write("<br> Suma: " + suma(3, 2));

var alcuadrado = x => x ** 2;

document.write("<br> Cuadrado: " + alcuadrado(3));

class Dato {
    constructor(x, y) {
        this.x = x;
        this.y = y;
    }
    mostrar() {
        document.write(`<br>Dato: x = ${this.x}, y = ${this.y}`)
    }
}
class Info extends Dato {
    constructor(x, y = 35, z = 20) {
        super(x, y)
        this.z = z;
    }
    mostrar() {
        super.mostrar();
        document.write(` z = ${this.z}`)
    }
}
let dato = new Dato("lo que quieras", 20);
dato.mostrar();
let info = new Info("lo que quieras");
info.mostrar();