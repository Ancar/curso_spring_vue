// JSON: Javascript Object Notation 
// Otra forma de crear objetos con la notacion JSON

let objetoVacio = {

};

let formaPago = {
    "modo": "TarjetaCredito",
    "comision": 2,
    "activa": true,
    "preparacion": null,
    "clientes": ["Santander", "Sabadell", "BBVA", [1, 2, 3, 4]],
    "configuracion": {
        "conexion": "ssl",
        "latencia": 15
    }
};
// let formaPago = JSON.parse(window.localStorage.getItem("datos-forma-pago"))
let matriz = [
    [4, 6, 8],
    [2, 3, 5],
    [4, 5, 1]
];

/*CONVERTIR UN OBJETO O ESTRUCTURA EN MEMORIA EN UN FORMATO TRANSMITIBLE
(O PARA ENVIAR POR RED O PARA GUARDAR EN FICHEROS)ES SERIALIZAR,
FORMATO PUEDE SER TEXTO (JSON, XML, YAML, UNO PROPIO), FORMATO BINARIO 
O ENCRIPTADO */
formaPago.servidor = "http//visa.com";
formaPago["oculta"] = "Dame 5 centimos para mi";
document.write(`<br>
<p>${formaPago.modo} ----- ${formaPago.clientes[1]}-----${formaPago.clientes[1][2]}</p>
matriz: ${matriz[2][1]} <br>
${JSON.stringify(formaPago)}
Usando forma de HashMap: ${formaPago["servidor"]}
 `);
window.localStorage.setItem("datos-forma-pago", JSON.stringify(formaPago));
let peticionUsuario = prompt("¿Que dato quieres?");
document.write(`<br> ${formaPago[peticionUsuario]}`);
let frutas = `[
    {
        "nombre":"pera", 
        "precio":2
    },
    {
        "nombre":"fresa", 
        "precio":4
    },
    {
        "nombre":"platano", 
        "precio":6
    }
]`;
//PARSEAR ES LA FORMA COLOQUIAL DE DECIR "LEER O INTERPRETAR" UN TEXTO
//PUEDE SER CONVERTIR UN TEXTO EN OTRO TEXTO, O EN ESTE CASO
//CONVIERTE UN TEXTO EN UN OBJETO O ESTRUCTURA EN MEMORIA. CUANDO HABLAMOS DEL ULTIMO CASO
//LA DEFINICION TECNICA ES DES-SERIALIZAR
let objFrutas = JSON.parse(frutas);
document.write(`<br>Fruta: ${objFrutas[1].nombre} Precio: ${objFrutas[1].precio}`);
// alert(JSON.stringify(formaPago));