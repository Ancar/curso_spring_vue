package com.grupoica.repasojava;

import com.grupoica.repasojava.modelo.Usuario;

public class EjemploMemoria {

	public static void pruebaPasoPorValor() {
	
		int x = 2;
		boolean y = false;
		String z = "Hola";
		funcionCualquiera(x, y, z);
		System.out.println("X = " + x + " Y = " + y +" Z = "+z);

	}
	
	private static void funcionCualquiera(int x, boolean y, String z) {
		
		System.out.println("X = " + x + " Y = " + y +" Z = "+z);
		x = 200;
		y = false;
		z = "Texto nuevo dentro de funcion";
		System.out.println("X = " + x + " Y = " + y +" Z = "+z);

	}
	
	public static void paruebaPasoPorReferencia() {
		
		Usuario alguien = new Usuario("Pepito", 30);
		int array []= new int[3];
		array[0] = 10; array[1] = 20; array[2] = 30;
		otraFuncion(alguien, array);
		int otroArray[] = array;
		otroArray[0] = 333;
		System.out.println("Nombre: "+ alguien.getNombre()+ ", primer elemento: " + array[0]);
		
	}
	private static void otraFuncion(Usuario parUsu, int []parArr) {
		
		System.out.println("Nombre: "+ parUsu.getNombre()+ ", primer elemento: " + parArr[0]);

		parUsu.setNombre("Modificao");
		parArr[0] = 999;
		
		System.out.println("Nombre: "+ parUsu.getNombre()+ ", primer elemento: " + parArr[0]);

		
	}
	
}
