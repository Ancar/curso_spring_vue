package com.grupoica.repasojava.modelo;

import java.util.ArrayList;

public class GestionUsuarios {

	
	//Todos los elementos son objetos con:
	//private ArrayList  listaUsuarios
	//Todos los elementos son del mismo tipo o de algun tipo heredero:
	private ArrayList <Usuario> listaUsuarios;

	public GestionUsuarios() {
		super();
		this.listaUsuarios = new ArrayList();
		//this.listaUsuarios.add(10);
		
	}
	public void listarUsuarios() {
		for(int i = 0; i<this.listaUsuarios.size(); i++) {
			System.out.println(this.listaUsuarios.get(i));
		}
	}
	public Usuario obtenerUsuarios(String nombre) {
		for(Usuario usu: listaUsuarios) {
			//if(usuObj instanceof Usuario) {
			//Usuario usu = (Usuario) usuObj;
				if(usu.getNombre().equals(nombre)) {
					return usu;
					//System.out.println("---"+usu.getNombre());
				}
			}
		return null;
		}
	//}
	public void eliminarUsuario(String nombre) {
		for(Usuario usu: listaUsuarios) {
			if(usu.getNombre().equals(nombre)) {
				
				listaUsuarios.remove(usu);
			}
		}
	}
	public void anadir(Usuario usu) {
		this.listaUsuarios.add(usu);
	}
	public Usuario modificarNombre(String nombre, String nuevoNombre) {
			
		for(Usuario usu: listaUsuarios) {
			if(usu.getNombre().equals(nombre)) {
				usu.setNombre(nuevoNombre);
				return usu;
			}
		}
		return null;
	}
	
	public Usuario modificarEdad(String nombre, int edad) {
		
		for(Usuario usu: listaUsuarios) {
			if(usu.getNombre().equals(nombre)) {
				usu.setEdad(edad);
				return usu;
			}
		}
		return null;
	}
	
	
	public Usuario modificarUsuarioEntero(String nombre, String  nuevoNombre, int edad) {
		for(Usuario usu: listaUsuarios) {
			
			if(usu.getNombre().equals(nombre)) {
				usu.setNombre(nuevoNombre);
				usu.setEdad(edad);
				return usu;
			}
		}
		return null;
	}
	
	public void eliminarUsuarios() {
		
		this.listaUsuarios.clear();
		
	}
	
	public void mostrarUsuariosPorEdad(int edad) {
		int cont = 0;
		for(Usuario usu: listaUsuarios) {
			if(usu.getEdad() == edad) {
				
				System.out.println("Usuario:"+ usu.getNombre() + ",Edad: "+ usu.getEdad() );
				
			}
			else {
				cont++;
			}
		}
		if(cont == listaUsuarios.size()) {
			System.out.println("Usuario no encontrado");
		}
		
	}
	
	public void mostrarUsuariosRangoEdad(int edad1, int edad2) {
		int cont = 0;
		for(Usuario usu: listaUsuarios) {
			if(usu.getEdad()>= edad1 && usu.getEdad()<= edad2) {
				System.out.println("Usuario:"+ usu.getNombre() + ",Edad: "+ usu.getEdad() );
			}else {
				cont++;
			}
			
		}
		if(cont == listaUsuarios.size()) {
			System.out.println("Usuario no encontrado");
		}
	}
	
	public void eliminarUnUsuario(String nombre) {
		
		for(Usuario usu: listaUsuarios) {
			if(usu.getNombre().equals(nombre)) {
				this.listaUsuarios.remove(usu);
				break;
			}
		}
	}
	
}
