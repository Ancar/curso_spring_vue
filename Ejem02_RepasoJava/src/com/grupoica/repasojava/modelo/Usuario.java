package com.grupoica.repasojava.modelo;

public class Usuario {

	private String nombre;
	private int edad;
	
	public Usuario() {
		nombre = "Sin nombre";
	}
	public Usuario(String nombre, int edad) {
		this.nombre = nombre;
		this.edad = edad;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	
	
	
	@Override
	public boolean equals(Object obj) {
		Usuario usuario = (Usuario)obj;
		return this.nombre == usuario.nombre && this.edad == usuario.edad;

	}
	//@Override
	public boolean equals(Usuario usuario) {
		return this.nombre == usuario.nombre && this.edad == usuario.edad;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return "Usuario " + nombre + ", edad: " + edad;
	}
	
	
}
