package com.grupoica.repasojava;

import java.util.HashMap;
import java.util.Scanner;

import com.grupoica.repasojava.modelo.Usuario;

public class EjemploHashMap {
	
	
	static HashMap<String, Usuario> diccUsuario;
	public static void probandoHashMap() {
	
		diccUsuario = new HashMap<>();
		diccUsuario.put("Luis", new Usuario("Luis", 18));
		diccUsuario.put("Ana", new Usuario("Ana", 20));
		diccUsuario.put("Luisa", new Usuario("Luisa", 30));
		Scanner sc = new Scanner(System.in);
		System.out.println("Introduzca el usuario");
		String nombre = sc.nextLine();
		System.out.println("El usuario es: "+diccUsuario.get(nombre).toString());

	}

}
