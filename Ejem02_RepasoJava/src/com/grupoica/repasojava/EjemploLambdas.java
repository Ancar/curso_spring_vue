package com.grupoica.repasojava;

public class EjemploLambdas {
	
	//UNA INTERFAZ ES FUNCIONAL CUANDO SOLO TIENE UN METODO
	interface NuestraFuncion{
		float operacion(float v, float w);
	}

	public static float sumar(float x, float y) {
		return x + y;
	}
	
	public static float resta(float x, float y) {
		return x - y;
	}
	
	public static float[] sumarArrays(float [] arr_1, float[] arr_2) {
	
		float [] arrRes = new float[arr_1.length];
		for(int i = 0; i< arr_1.length &&  i<arr_2.length; i++) {
			
			arrRes[i] = arr_1[i] + arr_2[i];
			
		}
		return arrRes;
		
	}
	
	public static float[] operarArrays(float [] arr_1, float[] arr_2, NuestraFuncion funCallback) {
		
		float [] arrRes = new float[arr_1.length];
		for(int i = 0; i< arr_1.length &&  i<arr_2.length; i++) {
			
			arrRes[i] = funCallback.operacion(arr_1[i], arr_2[i]);
			
		}
		return arrRes;
		
	}
	
	public static void ejecutarLambdas() {
		
		float [] a = {2,3,4};
		float [] b = {4,5,7};
		float [] r = sumarArrays(a, b);
		for(float f: r) {
			System.out.print(f);
		}
	
		System.out.println("");
		float [] r2 = operarArrays(a, b, EjemploLambdas::sumar);
		for(float f: r2) {
			System.out.println(f);
		}
		
		float [] mult = operarArrays(a, b, (float x, float y)->{
			System.out.println("Multiplicando " + x + " x "+ y + " = " + x*y);
			return x*y;
		});
		
		float [] r3 = operarArrays(a, b, EjemploLambdas::resta);
		for(float f: r3) {
			System.out.println(f);
		}
		
		float [] div = operarArrays(a, b, (float x, float y)->{
			System.out.println("Dividiendo " + x + " / "+ y + " = " + x/y);
			return x/y;
		});
		
		
		

	}
	
}
