package com.grupoica.repasojava.interfaces;

public class Patinete implements Motorizable {
	
	private String marca;
	private int bateria;
	
	public Patinete(String marca, int bateria) {
		
		this.marca = marca;
		this.bateria = bateria;
		
	}

	public int getBateria() {
		return bateria;
	}

	public void setBateria(int bateria) {
		this.bateria = bateria;
	}

	@Override
	public void encender() {
		// TODO Auto-generated method stub
		System.out.println("El patinete " + marca + " se ha encendido y le queda: " + bateria + " de bateria");
		
	}
	

}
