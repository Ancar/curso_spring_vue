package com.grupoica.repasojava.interfaces;

public class Perro implements Animal{

	private int edad;
	private String raza;
	public Perro(int edad, String raza) {
		this.edad = edad;
		this.raza = raza;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getRaza() {
		return raza;
	}

	public void setRaza(String raza) {
		this.raza = raza;
	}

	@Override
	public void alimentarse(String comida) {
		// TODO Auto-generated method stub
		
		System.out.println("El perro: " + raza + " ha comida tan ricamente " + comida);
		
	}
	
	
	
}
