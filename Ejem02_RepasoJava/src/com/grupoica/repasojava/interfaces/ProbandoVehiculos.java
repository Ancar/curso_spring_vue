package com.grupoica.repasojava.interfaces;

import java.util.ArrayList;

public class ProbandoVehiculos {

	
	public static void probar() {
		Coche miCoche = new Coche("Kia", 1500, 60.34f);
		miCoche.aceleracion();
		Coche miCocheFines = new Coche("Hammer", 2500, 60.34f);
		miCoche.aceleracion();
		
		Patinete miPatinete = new Patinete("Onda", 3);
		
		
		//Polimorfismo
		
		Vehiculo unVehiculo = miCoche; // Casting implicito
		Object unObjeto = miCoche;
		Coche unCoche = (Coche) unObjeto; // Casting explicito
		System.out.println(unObjeto.toString());
		unVehiculo.aceleracion();
		
		Caballo miCaballo = new Caballo("Pura Sangre", 200, 40);
		miCaballo.aceleracion();
		//ArrayList<Vehiculo> garaje = new ArrayList<>();
		
		ArrayList <Motorizable> garaje = new ArrayList<>();
		
		
		garaje.add(miPatinete);
		
		
		garaje.add(miCoche);
		garaje.add(miCocheFines);
		//garaje.add(miCaballo);
		//garaje.add(unVehiculo);
		//NO SE PUEDE
		//garaje.add(new Vehiculo("Que no he comprado", 30));
		
		for(Motorizable vehiculo: garaje) {
			//vehiculo.aceleracion();
			//vehiculo.desplazarse(1.5f);
			if (vehiculo instanceof Vehiculo) {
				Coche miCar = (Coche) vehiculo;
				miCar.aceleracion();
			}
			
			System.out.println("-------");
			vehiculo.encender();
		}
		
		
		ArrayList<Animal> granja = new ArrayList<>();
		
		granja.add(new Caballo("Raza buena", 300 , 35));
		granja.add(new Perro(2, "Husky"));
		
		for(Animal animales: granja) {
			
			if(animales instanceof Caballo) {
				animales.alimentarse("Heno");
			}
			if(animales instanceof Perro) {
				animales.alimentarse("Riskis");
			}
			
		}
		//miCoche.encender();
		//Motorizable vehMotor = miCoche;
		//vehMotor.encender();
	}
	
}
