package com.grupoica.repasojava.interfaces;

public class Caballo extends Vehiculo implements Animal {

	private int dientes;
	
	public Caballo(String marca, float peso, int dientes) {
		super(marca, peso);
		this.dientes = dientes;
	}

	public int getRaza() {
		return dientes;
	}

	public void setRaza(int dientes) {
		this.dientes = dientes;
	}

	@Override
	public void aceleracion() {
		// TODO Auto-generated method stub
		//super.aceleracion();
		System.out.println("Trotando mas rapido: " + marca + " con: " +dientes + " dientes");
	}

	@Override
	public void desplazarse(float distancia) {
		// TODO Auto-generated method stub
		
		System.out.println(marca + " galopa " + distancia + " metros");

		
	}

	@Override
	public void alimentarse(String comida) {
		// TODO Auto-generated method stub
		
		System.out.println("El caballo " + marca + " ha comido tan ricamente "+ comida);
		
	}
	
	
	
	
	

}
