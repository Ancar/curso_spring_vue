package com.grupoica.repasojava;

import com.grupoica.repasojava.modelo.GestionUsuarios;
import com.grupoica.repasojava.modelo.Loco;
import com.grupoica.repasojava.modelo.Usuario;
import com.grupoica.repasojava.EjemploHashMap;
import com.grupoica.repasojava.interfaces.ProbandoVehiculos;

public class ProgramaMain {

	public static void main(String[] args) {
		
		EjemploLambdas.ejecutarLambdas();
		
		ProbandoVehiculos.probar();
		
		/*
		GestionUsuarios gestionUsu = new GestionUsuarios();
		Usuario usu = new Usuario();
		usu.setEdad(30);
		usu.setNombre("Fulanito");
		Usuario usu2 = new Usuario("Paco", 20);
		System.out.println("Nombre: "+usu.getNombre() +", edad: "+usu.getEdad());
		System.out.println("Nombre: "+usu2.getNombre() + ", edad: "+usu2.getEdad());
		
		Usuario usu4 = new Usuario("Holiwis", 45);
		
		gestionUsu.anadir(usu4);
		
		gestionUsu.listarUsuarios();

		gestionUsu.anadir(new Usuario());
		gestionUsu.anadir(usu);
		gestionUsu.anadir(usu2);
		//this.listaUsuarios.add("Texto");	
		gestionUsu.listarUsuarios();
		if(usu2.equals(usu)) {
			System.out.println("Son iguales");
		}else {
			System.out.println("Son distintos");
		}
		Loco joker = new Loco();
		joker.setNombre("Joker");
		joker.setTipoLocura(true);
		if(joker.isTipoLocura()) {
			System.out.println("Esta loquisimo");
		}else{
			System.out.println("Bastante normal");
		}
		System.out.println("Joker: "+joker.toString());
		
		System.out.println(usu.equals(usu2));
		
		
		gestionUsu.listarUsuarios();
		//EjemploMemoria.paruebaPasoPorReferencia();
		//gestionUsu.listarUsuarios();
		
		System.out.println("CAMBIAMOS A PACO");
		System.out.println(gestionUsu.modificarUsuarioEntero("Paco", "El nuevo", 32));
		gestionUsu.listarUsuarios();
		
		System.out.println("CAMBIAMOS EL NOMBRE DE Holiwis");
		System.out.println(gestionUsu.modificarNombre("Holiwis", "Adios"));
		
		System.out.println("MOSTRAMOS LA LISTA DE NUEVO");
		
		gestionUsu.listarUsuarios();
		
		System.out.println("CAMBIAMOS LA EDAD DE FULANITO");
		
		System.out.println(gestionUsu.modificarEdad("Fulanito", 32));
		
		System.out.println("MOSTRAMOS LA LISTA PARA VER EL CAMBIO DE LA EDAD");
		gestionUsu.listarUsuarios();
		
		System.out.println("MOSTRAR POR EDAD");
		
		gestionUsu.mostrarUsuariosPorEdad(32);
		
		System.out.println("MOSTRAR POR RANGO DE EDAD");
		
		
		gestionUsu.mostrarUsuariosRangoEdad(30, 45);
		
		
		System.out.println("BORRAMOS UN UNICO USUARIO");
		
		gestionUsu.eliminarUnUsuario("Adios");
		
		System.out.println("VEMOS LA LISTA A VER SI ESTA ADIOS");
		
		gestionUsu.listarUsuarios();
		
		
		System.out.println("BORRAMOS USUARIOS");
		
		gestionUsu.eliminarUsuarios();
		
		System.out.println("Mostramos la lista entera a ver si queda alguno");
		
		gestionUsu.listarUsuarios();
		
		System.out.println("BORRADOS");
		EjemploHashMap.probandoHashMap();
		*/
		}

}
